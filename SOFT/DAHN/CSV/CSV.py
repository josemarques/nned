 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#CSV.py ->CSV data importer and exporter

##Base functions

def LF(filename):#Load file, return file on string format
	FILE= open(filename, "r")
	STRDATA=FILE.read()
	return STRDATA

def SF(filename, strdata):#Save string to file
    FILE = open(filename, "w+")
    FILE.write(strdata)
    FILE.close()

    ##File configuration save

def AF(filename, strdata):#Append string to file
	FILE= open(filename, "a")
	FILE.write(strdata)
	FILE.close()


def STTD(strdata, delim=","):#Encoded CSV string to vector string data
	RAW=[]

	RAW=strdata.splitlines( )
	#for line in LINES:
	#	RAW.append(line.rstrip())
	#print(RAW)
	DATA=[]
	for i in range(0,len(RAW)):#Line iterator
		rDATA=RAW[i].split(delim)
		DATA.append(rDATA)

	return DATA

def DTTS(data, delim=","):#String vector data to encoded string CSV
	STRDATA=""
	for i in range(len(data)):

		for ii in range(len(data[i])):

			STRDATA=str(STRDATA)+str(data[i][ii])
			if(ii<len(data[i])-1): STRDATA=str(STRDATA)+str(delim)

		STRDATA=str(STRDATA)+"\n"

	return STRDATA

def SVFV(data):#String vector to float vector
	FLDT=[]
	for i in range(len(data)):
		tFLDT=[]
		for ii in range(len(data[i])):
			try:
				FLOATV=float(data[i][ii])
			except:
				#print("An exception occurred")
				tFLDT.append(0)
			else:
				tFLDT.append(FLOATV)
		FLDT.append(tFLDT)
	return FLDT


def FVSV(data):#Float vector to string vector
	STDT=[]
	for i in range(len(data)):
		tSTDT=[]
		for ii in range(len(data[i])):
			tSTDT.append(str(data[i][ii]))
		STDT.append(tSTDT)
	return STDT

##Object implementation
"""
class CSV:
	def __init__(self, filename="", token=","):
		self.FILENAME=filename
		self.TOKEN=token
		self.CSVSTRING=LF(self.FILENAME)
		self.SDATA=STTD(LF(self.FILENAME))
		self.


	def GFD(self, filename="", token=""):#Get file data


	def PFD(self, filename="", token=""):#Put file data


	def UFD(self, filename="", token=""):#Update file data

"""
